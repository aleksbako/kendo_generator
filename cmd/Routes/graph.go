package routes

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	databT "gitlab.com/aleksbako/kendo_generator/database/Tournament"
)

func TournamenCharttInit(c echo.Context) error {
	id := c.Param("id")
	TournamentID, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println("error converting string to int")

	}

	specificTournament := databT.GetChartInitialTournament(TournamentID)
	return c.JSON(http.StatusOK, specificTournament)

}
