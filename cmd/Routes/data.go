package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
	databP "gitlab.com/aleksbako/kendo_generator/database/Player"
	databT "gitlab.com/aleksbako/kendo_generator/database/Tournament"
)

//Simple get all player objects
func RequestAllPlayers(c echo.Context) error {
	allPlayers := databP.GetAllPlayers()
	return c.JSON(http.StatusOK, allPlayers)
}

func RequestAllTournaments(c echo.Context) error {
	allTournaments := databT.GetAllTournaments()
	return c.JSON(http.StatusOK, allTournaments)

}
