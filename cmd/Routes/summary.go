package routes

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	databP "gitlab.com/aleksbako/kendo_generator/database/Player"
	databT "gitlab.com/aleksbako/kendo_generator/database/Tournament"
)

func RequestSpecificPlayerMatchTournamentData(c echo.Context) error {

	id := c.Param("id")
	playerID, err := strconv.Atoi(id)
	if err != nil {
		panic(err)
	}
	specificPlayerMatchTournament := databP.GetSpecificPlayerTournamentsMatches(playerID)
	return c.JSON(http.StatusOK, specificPlayerMatchTournament)

}

func RequestSpecificTournament(c echo.Context) error {

	id := c.Param("id")
	fmt.Println(id)
	TournamentID, err := strconv.Atoi(id)
	if err != nil {
		panic(err)
	}

	specificTournament := databT.GetTournament(TournamentID)

	return c.JSON(http.StatusOK, specificTournament)

}

//requestSpecificPlayer - fetches one player from database.
func RequestSpecificPlayer(c echo.Context) error {

	id := c.Param("id")

	playerID, err := strconv.Atoi(id)
	if err != nil {
		panic(err)
	}

	specificPlayer := databP.GetPlayer(playerID)

	return c.JSON(http.StatusOK, specificPlayer)

}
