
$(document).ready(function () {
    var baseUrl = (window.location).href; // You can also use document.URL
    var id = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
    let query = "id="+id

    let url = "http://localhost:8080/gettournament?" + query;
  
    //Send the URL and fetch the data back from the server
    fetch(url)
      .then((data) => data.json())
      .then((text) => {
        console.log("request succeeded with JSON response", text);
        $("#Tname").html(text.name)
        $("#Tyear").html(text.year)
        $("#Winner").html(text.winner_name)
        $("#playercount").html(text.pAmount)
        $("#countrycount").html(text.cAmount)
       ShowMatchesOfTournament(text.matches)
       drawPieChart(text.pieData)
      })
      .catch(function (error) {
        console.log("request failed", error);
      });


    
});

function ShowMatchesOfTournament(matches) {
    for (i = 0; i < matches.length; i++) {
        let link = document.createElement("a");
        link.setAttribute("class","list-group-item list-group-item-action")
        link.setAttribute("id",matches[i].match_id)
        link.textContent = matches[i].white_player + " vs "  + matches[i].red_player + " | " + " FF: " +  matches[i].typename + " | "+ " C: " + matches[i].comptypename;
        $(".list-group").append(link)
    };
  
  };

  $(document).on('click', '.list-group-item', function () {
    alert( "id of this match: " + this.id );
  });

  $(document).on('click','#detail-analysis',function(){
    console.log(window.location.href)
    window.location.href = window.location.href + "/chart";
    console.log(window.location.href)
  })
  
  
  function drawPieChart(data){
    // set the dimensions and margins of the graph
var width = 150
height = 150
margin = 10

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_dataviz'
var svg = d3.select("#my_dataviz")
.append("svg")
.attr("width", width)
.attr("height", height)
.append("g")
.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

// Create dummy data

// set the color scale
var color = d3.scaleOrdinal()
.domain(data)
.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b"])

// Compute the position of each group on the pie:
var pie = d3.pie()
.value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data))

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
.selectAll('whatever')
.data(data_ready)
.enter()
.append('path')
.attr('d', d3.arc()
.innerRadius(0)
.outerRadius(radius)
)
.attr('fill', function(d){ return(color(d.data.key)) })
.attr("stroke", "black")
.style("stroke-width", "2px")
.style("opacity", 0.7)

  }