function submit(e) {
  var li = document.getElementById("playerlist");
  var playerlist = li.getElementsByClassName("player");
  if (playerlist.length + 1 == 10) {
    console.log("testing")
    li.style.overflow = 'scroll';

  }



  var fn = document.getElementById("fname").value;
  var ln = document.getElementById("lname").value;
  var country = document.getElementById("country").value;
  var club = document.getElementById("club").value;
  var grade = document.getElementById("grade").value;
  for (i = 0; i < playerlist.length; i++) {
    if (
      playerlist[i].innerText ==
      fn + " " + ln + " " + country + " " + club + " " + grade
    ) {
      return;
    }
  }
  var node = document.createElement("button");


  node.innerText = fn + " " + ln + " " + country + " " + club + " " + grade;
  node.className = "list-group-item player mt-3";
  node.addEventListener("click", select);
  li.appendChild(node);
  console.log(grade);
  player = {
    firstname: fn,
    lastname: ln,
    country: country,
    club: club,
    grade: grade,
  };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/addplayer");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(player));


}

$("#submit").on('click', function (e) {
  e.preventDefault();
  submit();
  var description = document.getElementsByClassName("description");
  for (i = 0; i < description.length; i++) {
    description[i].style.display = "none"
  }
});



function deleteElement(node) {
  var li = document.getElementById("playerlist");
  var selectedlist = li.getElementsByClassName("selected");
  var playerlist = li.getElementsByClassName("player");

  if (playerlist.length < 10) {
    console.log("testing")
    li.style.overflow = '';
  }

  var xhr = new XMLHttpRequest();
  xhr.open("DELETE", "/deleteplayer");
  xhr.setRequestHeader("Content-Type", "application/json");
  for (var i = 0; i < selectedlist.length; i++) {
    var temp = selectedlist[i].innerText.split(" ");

    tempplayer = {
      Firstname: temp[0],
      Lastname: temp[1],
      Country: temp[2],
      Club: temp[3],
      Grade: temp[4] + " " + temp[5],
    };
    xhr.send(JSON.stringify(tempplayer));
    li.removeChild(selectedlist[i]);
  }
}

//select player in list
function select() {
  if (this.className == "list-group-item player selected") {
    this.className = "list-group-item player";
  } else {
    this.className += " selected";
  }
  document.getElementById('generatebtn').disabled = false;
  document.getElementById("delete").disabled = false
}



$("#generatebtn").on('click', function () {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/generatecomp");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send("request");
  console.log(xhr.response)
  window.location = "/competition";
});