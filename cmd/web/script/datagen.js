$(document).ready(function () {
  // Get all players and display them in a table when the page is loaded
  getAllPlayers().then((data) => {
    let players;
    players = data;
    listPlayers(players)
  })
})

// Display all players in a list
function listPlayers(players) {
  for (i = 0; i < players.length; i++) {
    var tableViewRow = document.createElement('tr');
    let tableViewIcon = document.createElement("td");
    let icon = document.createElement("i");
    tableViewIcon.appendChild(icon);
    icon.className = "fa fa-user-o";
    let tableViewName = document.createElement('td');
    let tableViewCountry = document.createElement('td');
    let tableViewClub = document.createElement('td');
    let button = document.createElement("td");
    let buttonAttr = document.createElement("button");
    buttonAttr.setAttribute("type", "button");
    buttonAttr.setAttribute("class", "player_list btn btn-info float-right")
    buttonAttr.textContent = "SELECT";
    buttonAttr.setAttribute('id', players[i].id);
    button.appendChild(buttonAttr);
    tableViewName.innerText = players[i].name;
    tableViewCountry.innerText = players[i].country;
    tableViewClub.innerText = players[i].club;

    tableViewRow.append(tableViewIcon, tableViewName, tableViewCountry, tableViewClub, button);
    document.getElementById("table-players").appendChild(tableViewRow);
  };

};

$(document).on('click', '.player_list', function () {
  getPlayer(this.id);
});

//<----------- GET REQUESTS FOR PLAYER/MATCH/TOURNAMENT/POINTS ------------->

//GET PLAYER
function getPlayer(id) {
  //Declare URL params object
  let params = {
    id: id
  };

  //Declare the query using paramscd
  let query = Object.keys(params)
    .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
    .join("&");

  //Send the URL and fetch the data back from the server
  const Http = new XMLHttpRequest();
  const url = window.location.hostname + "/getplayer?" + query;
  Http.open("GET", url);
  Http.send();

  Http.onreadystatechange = (e) => {
    window.location.href = "/getplayer?" + query;
    console.log(Http.responseText)
  }
};

//GET TOURNAMENT
$("#getTournamentSubmitBtn").on('click', function (e) {
  e.preventDefault();
  //Declare variables
  var name = document.getElementById("getTournamentName").value;
  var year = document.getElementById("getTournamentYear").value;

  //Declare URL params object
  let params = {
    name: name,
    year: year,
  };

  //Declare the query using params
  let query = Object.keys(params)
    .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
    .join("&");

  //Declare the request URL
  let url = "http://localhost:8080/tournamentresults?" + query;

  //Send the URL and fetch the data back from the server
  fetch(url)
    .then((data) => data.json())
    .then((text) => {
      //Ouput the response in UI
      var array = Object.values(text)
      d3.select("#getTournmentOutput")
        .selectAll("div.table")
        .data(array)
        .enter().append("div.table")
        .text(function (d) { return " " + d + " "; });
      console.log("request succeeded with JSON response", text);
    })
    .catch(function (error) {
      console.log("request failed", error);
    });
})

//GET MATCH
$("#getMatchSubmitBtn").on('click', function (e) {
  e.preventDefault();
  //Declare variables
  var tournament_id = document.getElementById("getMatchTId").value;
  var white_player_id = document.getElementById("getMatchWId").value;
  var red_player_id = document.getElementById("getMatchRId").value;
  var typename = document.getElementById("getMatchType").value;

  //Declare URL params object
  let params = {
    tournament_id: tournament_id,
    white_player_id: white_player_id,
    red_player_id: red_player_id,
    typename: typename,
  };

  //Declare the query using params
  let query = Object.keys(params)
    .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
    .join("&");

  //Declare the request URL
  let url = "http://localhost:8080/getmatch?" + query;

  //Send the URL and fetch the data back from the server
  fetch(url)
    .then((data) => data.json())
    .then((text) => {
      //Ouput the response in UI
      var array = Object.values(text)
      d3.select("#getMatchOutput")
        .selectAll("div.table")
        .data(array)
        .enter().append("div.table")
        .text(function (d) { return " " + d + " "; });
      console.log("request succeeded with JSON response", text);
    })
    .catch(function (error) {
      console.log("request failed", error);
    });
})

//GET POINTS
$("#getPointsSubmitBtn").on('click', function (e) {
  e.preventDefault();
  //Declare variables
  var match_id = document.getElementById("getPointsMatchId").value;
  var player_id = document.getElementById("getPointsPlayerID").value;

  //Declare URL params object
  let params = {
    match_id: match_id,
    player_id: player_id,
  };

  //Declare the query using params
  let query = Object.keys(params)
    .map((k) => encodeURIComponent(k) + "=" + encodeURIComponent(params[k]))
    .join("&");

  //Declare the request URL
  let url = "http://localhost:8080/getpoints?" + query;

  //Send the URL and fetch the data back from the server
  fetch(url)
    .then((data) => data.json())
    .then((text) => {
      //Ouput the response in UI
      var array = Object.values(text)
      d3.select("#getPointsOutput")
        .selectAll("div.table")
        .data(array)
        .enter().append("div.table")
        .text(function (d) { return " " + d + " "; });
      console.log("request succeeded with JSON response", text);
    })
    .catch(function (error) {
      console.log("request failed", error);
    });
})

//<----------- DATA ANALYSIS ------------->


// DATA ANALYSIS CATEGORIES

//Players menu item - Get's a list of all players
$("#players").on('click', function (e) {
  $("#players").addClass("active");
  // Remove the active class from any other dropdown list selected
  active("players");
  //Get all players
  getAllPlayers().then((data) => {
    let players;
    players = data;
    listPlayers(players)
  })
})


//Tournaments menu item - Fetch all tournaments when option is selected
$("#tournament").on('click', function (e) {
  $("#tournament").addClass("active");

  // Remove the active class from any other dropdown list selected
  active("tournament");

  // e.preventDefault();
  getAllTournaments();
})

function player(name, country, club, id) {
  this.name = name;
  this.country = country;
  this.club = club;
  this.id = id;
}

// REQUESTS 
//Get all players
async function getAllPlayers() {
  //Declare the request URL
  let url = "http://localhost:8080/getallplayers"
  //Send the URL and fetch the data back from the server
  const playersList = [];
  return fetch(url)
    .then((data) => data.json())
    .then((text) => {
      var name;
      var country;
      var club;
      var id;
      //save all players data in the playersList array of obj
      for (i = 0; i < text.length; i++) {
        name = text[i].name;
        country = text[i].country;
        club = text[i].club;
        id = text[i].player_id;
        playersList.push(new player(name, country, club, id))
      }
      console.log("request succeeded with JSON response", text);
      return playersList

    })
    .catch(function (error) {
      console.log("request failed", error);
    });

}
//Fetch all tournaments
function getAllTournaments() {
  //Declare the request URL
  let url = "http://localhost:8080/getalltournaments"
  //Send the URL and fetch the data back from the server
  fetch(url)
    .then((data) => data.json())
    .then((text) => {
      var getName;
      var getYear;
      tablet = text;

      for (i = 0; i < text.length; i++) {
        //Get name/country/age from the json object and join the sperated characters into a string
        getName = Object.values(text[i].name);
        let name = getName.join("")
        getYear = Object.values(text[i].year);
        let year = getYear.join("");
        tournamentArray.push([name, year])

      }
      console.log("request succeeded with JSON response", text);
      console.log(tournamentArray);
    })
    .catch(function (error) {
      console.log("request failed", error);
    });
}

//----------------------------------------------------

//Submit function returns player ID
function submit() {
  console.log("printing Table")
  console.log(table)
  console.log(selectedName)
  var id;
  for (i = 0; i < table.length; i++) {
    if (table[i].name == selectedName && table[i].birth_date == selectedAge && table[i].country == selectedCountry) {
      id = table[i].player_id;
      console.log(table)
      break;
    }
  };
  console.log(id)
  return id;
}

// Remove the active class from any other dropdown list selected
function active(id) {
  const dropdownItem = document.getElementsByClassName("dropdown-item");
  for (i = 0; i < dropdownItem.length; i++) {
    console.log(dropdownItem[i].className)
    if (dropdownItem[i].className == "dropdown-item active" && dropdownItem[i].id != id) {
      console.log("it goes here")
      dropdownItem[i].classList.remove("active")
    }
  }

}
//HISTOGRAM CODE
function histogram(object) {

  console.log("hello")
  $("#d3-container").empty();

  const data = object
  const width = 600;
  const height = 550;
  const margin = { top: 50, bottom: 50, left: 50, right: 50 };
  console.log(data)
  const svg = d3.select('#d3-container')
    .append('svg')
    .attr('width', width - margin.left - margin.right)
    .attr('height', height - margin.top - margin.bottom)
    .attr("viewBox", [0, 0, width, height]);

  const x = d3.scaleBand()
    .domain(d3.range(data.length))
    .range([margin.left, width - margin.right])
    .padding(0.1)

  const arrx = d3.scaleBand()
    .domain(d3.range(4))
    .range([margin.left, width - margin.right])
    .padding(0.1)

  console.log(typeof x)

  const y = d3.scaleLinear()
    .domain([0, 100])
    .range([height - margin.bottom, margin.top])

  if (data[0].score != undefined) {
    svg
      .append("g")
      .attr("fill", 'royalblue')
      .selectAll("rect")
      .data(data)
      .join("rect")
      .attr("x", (d, i) => x(i))
      .attr("y", d => y(d.score))
      .attr('title', (d) => d.score)
      .attr("class", "rect")
      .attr("height", d => y(0) - y(d.score))
      .attr("width", x.bandwidth());
  }
  if (data[0].max_points != undefined) {
    svg
      .append("g")
      .attr("fill", 'royalblue')
      .selectAll("rect")
      .data(data)
      .join("rect")
      .attr("x", (d, i) => x(i))
      .attr("y", d => y(d.max_points))
      .attr('title', (d) => d.max_points)
      .attr("class", "rect")
      .attr("height", d => y(0) - y(d.max_points))
      .attr("width", x.bandwidth());
  }


  if (data[0].sum_men != undefined && data[0].sum_kote != undefined) {
    var temp = Object.values(data[0])
    temp.length = temp.length - 1
    svg
      .append("g")
      .attr("fill", 'royalblue')
      .selectAll("rect")
      .data(temp)
      .join("rect")
      .attr("x", (d, i) => arrx(i))
      .attr("y", (d) => y(d))
      .attr('title', (d) => d)
      .attr("class", "rect")
      .attr("height", (d) => y(0) - y(d))
      .attr("width", arrx.bandwidth());
  }





  function yAxis(g) {
    g.attr("transform", `translate(${margin.left}, 0)`)
      .call(d3.axisLeft(y).ticks(null, data.format))
      .attr("font-size", '1em')
  }

  function xAxis(g) {
    if (data[0].score != undefined) {
      g.attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickFormat(i => data[i].name + " " + data[i].year))
        .attr("font-size", '1em')
    }
    else if (data[0].max_points != undefined) {

      g.attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickFormat(i => data[i].name + " " + data[i].max_type_name))
        .attr("font-size", '1em')
    }

    else if (data[0].sum_men != undefined && data[0].sum_kote != undefined) {
      var temp = ["Men", "Kote", "Do", "Tsuki"]
      g.attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(arrx).tickFormat(i => temp[i]))
        .attr("font-size", '1em')
    }
  }

  svg.append("g").call(xAxis);
  svg.append("g").call(yAxis);
  svg.node();
};

$("#download-datagen").on('click', function () {
  var arr = [];
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/getdataAll");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send("request");
  var obj
  xhr.onload = function () {

    json = JSON.parse(xhr.response);
    obj = json

    for (var i = 0; i < json.length; i++) {
      arr.push([json[i].name, json[i].country, json[i].club, json[i].grade, json[i].height, json[i].date, json[i].tname, json[i].tyear, json[i].mtype, json[i].comptype, json[i].tpoints, json[i].mpoints, json[i].kpoints, json[i].dpoints, json[i].tskpoints]);
    }

    let csvContent = "data:text/csv;charset=utf-8,"
    headers = ["Full Name", "Country", "Club", "Grade", "Height", "Age", "Tournament Name", "Tournament Year", "Match Type", "Competition Type", "Total Match Points", "Men Points", "Kote Points", "Do Points", "Tsuki Points"]
    csvContent += headers + "\t\r\n"
    arr.forEach(function (rowArray) {
      let row = rowArray.join(",");
      csvContent += row + "\r\n";

    });

    var encodedUri = encodeURI(csvContent);
    console.log(csvContent)
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "my_data.csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named "my_data.csv".
  }
  console.log(arr)

  const rows = [
    ["name1", "city1", "some other info"],
    ["name2", "city2", "more info"]
  ];

});
