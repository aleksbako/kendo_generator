import React from "react";
import DataTable from "./DataTable";
import NavigationBar from "../NavigationBar.js"
import SideBar from "../SideBar";

import {Container, Col, Row} from 'react-bootstrap';
function Data(props){
 
    return (<>
        <NavigationBar />
        <SideBar />
       
        <DataTable List={props.List} dtype={props.dtype} />
     
    </>)
}
 export default Data;