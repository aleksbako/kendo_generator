import React, { Component } from "react";
import { Container, Col, Row } from 'react-bootstrap';
import DataListElement from "./DataListElement";

class DataTable extends Component {

  constructor() {
    super();
    this.state = { list: [] }
  }



  render() {
    const List = this.props.List.map(data => {
      if (this.props.dtype == "player") {
        return <DataListElement key={data.id} id={data.id} name={data.name} country={data.country} club={data.club} dtype={this.props.dtype} />
      }
      else {
        return <DataListElement key={data.id} id={data.id} name={data.name} year={data.year} dtype={this.props.dtype} />
      }
    })


    return (
      <Container fluid="md">
        <Row>
          <Col>
            <div class="form-group mx-5">
              <input type="text" class="form-control" id="search" name="search" aria-describedby="dateHelp"
                placeholder="Search" />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <table class="table table-striped mx-5 table-hover">
              <thead class="thead-dark">

                {
                  this.props.dtype == "player" ?
                    <tr>
                      <th scope="col"><i class="fa fa-user-o"></i> Player</th>
                      <th scope="col">Name</th>
                      <th scope="col">Country</th>
                      <th scope="col">Club</th>
                      <th scope="col"></th>
                    </tr>
                    :
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Year</th>
                      <th scope="col"></th>
                    </tr>
                }

              </thead>
              <tbody id="table-players" class="mb-5">
                {List}
              </tbody>
            </table>
          </Col>
        </Row>
      </Container>
    );
  }
}
export default DataTable;
