import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from 'react-bootstrap';

class DataListElement extends Component {
    constructor(props) {
        super();
        this.state = {
            id: 0,
            name: "",
            country: "",
            club: "",
            year: ""
        }

    }
    componentDidMount() {
        if (this.props.dtype == "player") {

            this.setState({
                id: this.props.id,
                name: this.props.name,
                country: this.props.country,
                club: this.props.club
            })
        }
        else {
            this.setState({
                id: this.props.id,
                name: this.props.name,
                year: this.props.year,

            })
        }


    }
    render() {

        if (this.props.dtype == "player")
            return (
                <tr>
                    <td name="tableIcon">
                        <i></i>
                    </td>
                    <td name="tablename">
                        {this.state.name}
                    </td>
                    <td name="tableCountry">
                        {this.state.country}
                    </td>
                    <td name="tableClub">{this.state.club}</td>
                    <Link to={{
                        pathname: "/data/players/specific/" + this.state.id, state: {
                            requestval: "player",
                            id: this.state.id
                        }
                    }} >
                        <Button>Select</Button></Link>
                </tr>
            );
        else {
            return (


                <tr>


                    <td name="tablename">
                        {this.state.name}
                    </td>

                    <td name="tableYear">
                        {this.state.year}
                    </td>

                    <Link to={{
                        pathname: "/data/tournaments/specific/" + this.state.id, state: {
                            requestval: "tournament",
                            id: this.state.id
                        }
                    }} >
                        <Button>Select</Button></Link>
                </tr>


            )
        }
    }

}
export default DataListElement;