import React from "react"
import NavigationBar from "../NavigationBar"
import SideBar from "../SideBar";

function DataMain() {
    return (
        <div>
            <NavigationBar />
            <SideBar />
        </div>
    )
}

export default DataMain