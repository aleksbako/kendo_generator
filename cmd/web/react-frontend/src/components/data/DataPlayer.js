
import React, { Component } from "react";
import Data from "./Data";
import "../style/datagen.css"
class DataPlayer extends Component {
  constructor() {
    super();
    this.state = { List: [] };
  }

  componentDidMount() {
    // this.setState({List: [{id:1,name:"aleks",country:"norway",club:"osi"}]})
    let url = "http://localhost:8080/getallplayers"

    const playersList = [];
    fetch(url)
      .then((data) => data.json())
      .then((text) => {
        var name;
        var country;
        var club;
        var id;
        //save all players data in the playersList array of obj
        for (let i = 0; i < text.length; i++) {
          id = text[i].player_id;
          name = text[i].name;
          country = text[i].country;
          club = text[i].club;
          playersList.push({ id, name, country, club })
        }
        console.log("request succeeded with JSON response", text);

        this.setState({ List: playersList })


      })
      .catch(function (error) {
        console.log("request failed", error);
      });
  }

  render() {

    return (<Data List={this.state.List} dtype="player" />)
  }

}
export default DataPlayer;