import React, { Component } from "react";
import Data from "./Data";
import "../style/datagen.css"

class DataTournament extends Component {
  constructor() {
    super();
    this.state = {
      List: []
    }
  }

  componentDidMount() {
    console.log("testing")
    //Declare the request URL
    let url = "http://localhost:8080/getalltournaments"
    //Send the URL and fetch the data back from the server
    var tournaments = [];
    return fetch(url)
      .then((data) => data.json())
      .then((text) => {
        var getId;
        var getName;
        var getYear;
        //tablet = text;
        for (let i = 0; i < text.length; i++) {

          //Get name/country/age from the json object and join the sperated characters into a string
          getId = text[i].t_id;

          getName = text[i].name;

          getYear = text[i].year;

          tournaments.push({ id: getId, name: getName, year: getYear })
        }

        console.log("request succeeded with JSON response", text);
        this.setState({
          List: tournaments
        });

      })

      .catch(function (error) {
        console.log("request failed", error);
      });
  }

  render() {

    return (
      <Data List={this.state.List} dtype="tournament" />
    );
  }
}
export default DataTournament