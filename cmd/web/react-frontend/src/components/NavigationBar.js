import {Container, Row, Col,  Nav,  Form} from 'react-bootstrap';

function NavigationBar(props){
    return (
      <>
      <Container fluid>
      <Row>
        <Col>
          <Nav className="navbar navbar-expand-lg navbar-light navbar-custom pt-0 mt-0">
            <button className="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse">
              <span className="navbar-toggler-icon my-toggler"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-between" id="navbarCollapse">
              <ul className="navbar-nav mr-auto " id="topmenu">
                <div className="navbar-nav">
                  <li className="nav-item ">
                    <a className="nav-link mx-2" href="/">Main </a>
                  </li>
                  <li className="nav-item active">
                    <a className="nav-link mx-2" href="/data">Data analysis <span class="sr-only">(current)</span></a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link mx-2" href="/generate">Generator</a>
                  </li>
                
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle mx-2" href="http://example.com" id="navbarDropdownMenuLink"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Login
                    </a>
                    <div className="dropdown-menu" style={{padding: "15px", paddingBottom: "10px"}}>
                      <Form className="form-horizontal" method="post" accept-charset="UTF-8" action="/login">
                        <input className="form-control login" type="name" name="username" placeholder="Username.."></input><br />
                        <input className="form-control login" type="password" name="password" placeholder="Password.."></input><br />
                        <input className="btn btn-primary" type="submit" name="submit" value="Login"></input>
                      </Form>
                    </div>
                  </li>
                  
                  <div className="navbar-nav">
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="/logout">Logout</a>
                    </li>
                    <li className="nav-item">
                      <div id="downlaodbtn">
                        <button className="btn btn-primary ml-5 mt-4" id="download-datagen" type="button">Download
                          All
                          Data</button>
                      </div>
                    </li>
                  </div>
                </div>
              </ul>
            </div>
          </Nav>
        </Col>
      </Row>
    </Container>
    </>
        
    );

}
export default NavigationBar