import React from "react"
import { Container, Col, Row } from 'react-bootstrap';

function PlayerSelected(props) {

         return (
             <div>
                 <Row>
                     <Col>
                         <h1>Player</h1>
                    </Col>
                 </Row>
                 <Row>
                     <Col>
                         <h2 >Name:
                         <a id="Pname">{" " + props.data.name}</a>
                         </h2>
                     </Col>

                 </Row>
                 <Row>
                     <Col>
                         <h2 >
                             Country:
                         <a id="PCountry">{" " + props.data.country}</a>
                         </h2>
                     </Col>
                 </Row>
                 <Row>
                     <Col>
                         <h2 >Club:
                     <a id="PClub">{" " + props.data.club}</a>
                         </h2>
                     </Col>
                 </Row>
                 <Row>
                     <Col>
                         <h2 >Grade:
                     <a id="PGrade">{" " + props.data.grade}</a>
                         </h2>
                     </Col>
                 </Row>
                 <Row>
                     <Col>
                         <h2 >Height:
                     <a id="PHeight">{" " + props.data.height}</a>
                         </h2>
                     </Col>
                 </Row>
            </div>
         )
     }
export default PlayerSelected