import React from "react"
import { Container, Col, Row, Button } from "react-bootstrap"
import { useParams, Link } from "react-router-dom";

function PlayerSummary(props) {
    return (
        <div>
            <Row>
                <Col>
                    <h2 class="text-center">Summary</h2>
                </Col>
            </Row>
            <Row>
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th class="text-center" scope="col"><Link to={"/data/players/specific/graph/" + props.id}><Button>Graph analysis</Button></Link></th>
                            <th class="text-center" scope="col"><a id="test" href="#"></a></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">Number Of Tournaments</th>
                            <th class="text-center" scope="col">Number of Matches</th>
                            <th class="text-center" scope="col">Each Type of Point Ratio</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th class="text-center" scope="col" id="countrycount"></th>
                            <th class="text-center" scope="col" id="playercount"></th>
                            <th class="text-center" scope="col" ><div id="my_dataviz"></div></th>
                        </tr>
                    </thead>
                </table>
            </Row>
        </div>
    )
}

export default PlayerSummary