import React from "react"
import SummaryPage from "./SummaryPage"

class PsummaryPage extends React.Component {
    constructor() {
        super()
        this.state = {
            id: 0,
            name: "",
            country: "",
            club: "",
            grade: "",
            height: "",
            birthdate: "",
            player: false

        }
    }
    componentWillMount() {
        console.log(`this is before if ${this.state.id}`)
        if (this.state.id == 0) {
            console.log(this.props.match.params.id)
            let id = this.props.match.params.id
            this.setState({
                id: id
            })
        }
        let query = "id=" + this.props.match.params.id
        console.log(query)
        let url = "http://localhost:8080/getplayer/" + this.props.match.params.id;
        
        //Send the URL and fetch the data back from the server
        fetch(url)
            .then((data) => data.json())
            .then((text) => {
                console.log("request succeeded with JSON response", text);
                console.log(text.player_id);
               
                this.setState({
                    
                    id: text.player_id,
                    name: text.name,
                    country: text.country,
                    club: text.club,
                    grade: text.grade,
                    height: text.height,
                    birthdate: text.birthdate,
                    player: true
                })
                //drawPieChart(text.pieData)
            })
            .catch(function (error) {
                console.log("request failed", error);
            });
    }
    render() {
      
        return (
            <SummaryPage key={this.state.id} player={this.state} type="player"/>
            //<h1>hi</h1>
        )
    }

}

export default PsummaryPage