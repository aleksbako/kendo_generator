import React, { Component } from "react"
import { Container, Col, Row } from 'react-bootstrap'
import PlayerSelected from "./Player/PlayerSelected"
import PlayerSummary from "./Player/PlayerSummary"
import MatchList from "./Tournament/MatchList"
import TournamentSelected from "./Tournament/TournamentSelected"
import TournamentSummary from "./Tournament/TournamentSummary"

function SummaryPage(props) {
  var val;
  if(props.type == "player"){
    val = (<div>
        <PlayerSelected data={props.player} />
        <PlayerSummary id={props.player.id}/>
      </div>)
  }
  else if (props.type == "tournament"){
    val = (<div>
      <TournamentSelected tournament={props.tournaments} />
      <TournamentSummary id={props.tournaments.id} />
      <MatchList />
    </div>)
  }
  return (
    < Container >
      {val}
    </Container >
  )
}
export default SummaryPage