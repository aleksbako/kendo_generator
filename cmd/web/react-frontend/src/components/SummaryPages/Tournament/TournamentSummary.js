import React from "react"
import {  Col, Row , Button} from "react-bootstrap"
import {  Link, useParams } from "react-router-dom";

function TournamentSummary(props) {
    return (
        <div>
            <Row>
                <Col>
                    <h2 class="text-center">Summary</h2>
                </Col>
            </Row>
            <Row>
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th class="text-center" scope="col"><Link to={"/data/tournaments/specific/graph/" + props.id}><Button>Graph analysis</Button></Link></th>
                            <th class="text-center" scope="col"><a id="test" href="#"></a></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th class="text-center" scope="col">Number Of Countries</th>
                            <th class="text-center" scope="col">Number of Participants</th>
                            <th class="text-center" scope="col">Each Type of Point Ratio</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th class="text-center" scope="col" id="countrycount"></th>
                            <th class="text-center" scope="col" id="playercount"></th>
                            <th class="text-center" scope="col" ><div id="my_dataviz"></div></th>
                        </tr>
                    </thead>
                </table>
            </Row>
        </div>
    )
}

export default TournamentSummary