import React from "react"
import { Container, Col, Row } from 'react-bootstrap';

function TournamentSelected(props) {

    return (
        <div>
            <Row>
                <Col>
                    <h1>Tournament</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h2 >Name:
                            <a id="Tname">{props.tournament.name}</a>
                    </h2>
                </Col>

            </Row>
            <Row>
                <Col>
                    <h2 >
                        Year:
                            <a id="Tyear">{props.tournament.year}</a>
                    </h2>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h2 >Winner:
                            <a id="Winner">{props.tournament.winner_name}</a>
                    </h2>
                </Col>
            </Row>
        </div>
    )
    // }
}

export default TournamentSelected