import React from "react"
import SummaryPage from "./SummaryPage"

class TsummaryPage extends React.Component {
    constructor() {

        super();
        this.state = {

            id: 0,
            name: "",
            year: "",
            winner_name: "",
            pAmount: 0, //Replace with other variable name
            cAmount: 0, // replace variable name to be reused for players as well
            firstlist: [],
            secondlist: []
        }
    }
    componentDidMount() {
        if (this.state.id == 0) {
            let id = this.props.match.params.id
            this.setState({
                id: id
            })
        }
        
         let query = "id="+ this.props.location.state.id
         console.log(query)
         let url = "http://localhost:8080/gettournament/" + this.props.match.params.id;
       
         //Send the URL and fetch the data back from the server
         fetch(url)
           .then((data) => data.json())
           .then((text) => {
             console.log("request succeeded with JSON response", text);
             this.setState({
             name: text.name,
             year: text.year,
         winner_name: text.winner_name,
         pAmount: text.pAmount, 
         cAmount: text.cAmount, 
         firstlist : text.matches,
         
          
         })
            //drawPieChart(text.pieData)
           })
           .catch(function (error) {
             console.log("request failed", error);
           });
    }

    render() {
        console.log(this.state)
        let state = this.state
        return (

            <SummaryPage tournaments={state} type="tournament" />
          
        )
    }
}
export default TsummaryPage