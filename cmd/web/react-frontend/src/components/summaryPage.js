import React, {Component} from "react"

import {Link} from "react-router-dom";
import {Container, Col, Row, Button} from 'react-bootstrap';
class summaryPage extends Component{
    constructor(){
      
        super();
        this.state = {
            
            id: 0,
            name: "",
            year: "",
            winner_name: "",
            pAmount: 0, //Replace with other variable name
            cAmount: 0, // replace variable name to be reused for players as well

            //Variables for players
            country: "",
            club: "",
            height: 0,
          
            firstlist : [],
            secondlist: []
        }
    }


    componentDidMount(){
        if(this.state.id==0){
            let id = this.props.match.params.id
            this.setState({
                id: id
            })
        }
        if(this.props.location.state.requestval == "tournament"){
            var baseUrl = (window.location).href; // You can also use document.URL
           // var id = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
            let query = "id="+ this.props.location.state.id
            console.log(query)
            let url = "http://localhost:8080/gettournament/" + this.props.match.params.id;
          
            //Send the URL and fetch the data back from the server
            fetch(url)
              .then((data) => data.json())
              .then((text) => {
                console.log("request succeeded with JSON response", text);
                this.setState({
                name: text.name,
                year: text.year,
            winner_name: text.winner_name,
            pAmount: text.pAmount, 
            cAmount: text.cAmount, 
            firstlist : text.matches,
            
             
            })
               //drawPieChart(text.pieData)
              })
              .catch(function (error) {
                console.log("request failed", error);
              });
        }
        else if(this.props.location.state.requestval == "player"){
          var baseUrl = (window.location).href; // You can also use document.URL
          // var id = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
           
           let url = "http://localhost:8080/getplayer/" + this.props.match.params.id;
         
           //Send the URL and fetch the data back from the server
           fetch(url)
             .then((data) => data.json())
             .then((text) => {
               console.log("request succeeded with JSON response", text);
               this.setState({
                name: text.name,
                year: text.birth_date,
            height: text.height,
            club: text.club, 
            country: text.country, 
            grade : text.grade,
           
            
           })
              //drawPieChart(text.pieData)
             })
             .catch(function (error) {
               console.log("request failed", error);
             });
        }
    }

    render(){
        if(this.props.location.state.requestval == "player"){
           
         return( <Container>
               
          <Row>
              <Col>
                  <h1>Player</h1>
                  </Col>
            
          </Row>
          <Row>
          
              <Col>
                  <h2 >Name:  
                  <a id="Tname">{this.state.name}</a>
                  </h2>
                  </Col>
             
          </Row>
          <Row>
              
              <Col>
                <h2 >
                  Year:
                  <a id="Tyear">{this.state.year}</a>
                </h2>
                
              </Col>
             
          </Row>
          <Row>
           
              <Col><h2 >Winner:<a id="Winner">{this.state.country}</a></h2></Col>
             
          </Row>
          <Row>
          
            <Col>
                  <h2 class="text-center">Summary</h2>
            </Col>
          </Row>
          <Row>
          <table class="table table-bordered">
              
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th class = "text-center" scope="col"><Link to={"/data/player/specific/graph/" + this.state.id} > <Button>Graph analysis</Button></Link></th>
                  <th class = "text-center" scope="col"></th>
                
                </tr>
                
              </thead>
              
              <thead>
                  <tr>
                    <th class = "text-center" scope="col">Grade</th>
                    <th class = "text-center"scope="col">Club</th>
                    <th class = "text-center"scope="col">Height</th>
                  </tr>
                </thead>
                <thead>
                  <tr>
                    <th class="text-center" scope="col" id="countrycount"></th>
                    <th class="text-center" scope="col" id="playercount"></th>
                    <th class = "text-center" scope="col" ><div id="my_dataviz"></div></th>
                  </tr>
              </thead>
          </table>
                  
              </Row>
              <Row>
                  <div class="col"></div>
                  <div class="col">
                 <h2>Match-List</h2>
                 </div>
                  <div class="col"></div>
              </Row>
              <Row>
                <h5>FF - Fraction of final, C - Class</h5>
              </Row>
              <Row>
                  <div class="col"></div>
                  <div class="col">
                  <div class="list-group">
                  
                    </div>
                  </div>
                    <div class="col"></div>
              </Row>
      </Container>
      )
        }
        if(this.props.location.state.requestval == "tournament"){
        return(
            
            <Container>
               
            <Row>
                <Col>
                    <h1>Tournament</h1>
                    </Col>
              
            </Row>
            <Row>
            
                <Col>
                    <h2 >Name:  
                    <a id="Tname">{this.state.name}</a>
                    </h2>
                    </Col>
               
            </Row>
            <Row>
                
                <Col>
                  <h2 >
                    Year:
                    <a id="Tyear">{this.state.year}</a>
                  </h2>
                  
                </Col>
               
            </Row>
            <Row>
             
                <Col><h2 >Winner:<a id="Winner">{this.state.winner_name}</a></h2></Col>
               
            </Row>
            <Row>
            
              <Col>
                    <h2 class="text-center">Summary</h2>
              </Col>
            </Row>
            <Row>
            <table class="table table-bordered">
                
                <thead>
                  <tr>
                    <th scope="col"></th>
                    <th class = "text-center" scope="col"><Link to={"/data/tournaments/specific/graph/" + this.state.id} > <Button>Graph analysis</Button></Link></th>
                    <th class = "text-center" scope="col"></th>
                  
                  </tr>
                  
                </thead>
                
                <thead>
                    <tr>
                      <th class = "text-center" scope="col">Number Of Countries</th>
                      <th class = "text-center"scope="col">Number of Participants</th>
                      <th class = "text-center"scope="col">Each Type of Point Ratio</th>
                    </tr>
                  </thead>
                  <thead>
                    <tr>
                      <th class="text-center" scope="col" id="countrycount"></th>
                      <th class="text-center" scope="col" id="playercount"></th>
                      <th class = "text-center" scope="col" ><div id="my_dataviz"></div></th>
                    </tr>
                </thead>
            </table>
                    
                </Row>
                <Row>
                    <div class="col"></div>
                    <div class="col">
                   <h2>Match-List</h2>
                   </div>
                    <div class="col"></div>
                </Row>
                <Row>
                  <h5>FF - Fraction of final, C - Class</h5>
                </Row>
                <Row>
                    <div class="col"></div>
                    <div class="col">
                    <div class="list-group">
                    
                      </div>
                    </div>
                      <div class="col"></div>
                </Row>
        </Container>
        )
        }
    }
}
export default summaryPage