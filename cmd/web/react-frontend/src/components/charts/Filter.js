import React  from "react";
import {Link } from "react-router-dom";
import {Button, Navbar, Nav, NavDropdown, Form, FormControl, Row, Col,Collapse} from 'react-bootstrap';
import RangeSlider from 'react-bootstrap-range-slider';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import Slider from "./Slider"
function Filter(props) {
  
if(props.type == "tournament"){
    return (<Form>
        <Form.Group controlId="formBasicEmail">
          
         
          <Form.Label style={{color: 'white'}}>Select Player:</Form.Label>
          <Form.Control type="player" placeholder="Enter requested players name" />
      
        </Form.Group>
      
        
        <Form.Group controlId="formCountry">
        <Form.Label style={{color: 'white'}}>Country:</Form.Label>
        <Form.Control as="select">
          <option>Select Country</option>
      
          
        </Form.Control>
        <Form.Label style={{color: 'white'}}>Club:</Form.Label>
        <Form.Control as="select">
        <option key = 'blankChoice' hidden value> Select Club </option>
          <option>Clubs</option>
        </Form.Control>
        </Form.Group>
      
        <Form.Label style={{color: 'white'}}>Min age:</Form.Label>
       
            <Slider />
             
            <Form.Label style={{color: 'white'}}>Max age:</Form.Label>
        
            <Slider />
        <Form.Group controlId="formAllCheckbox">
        <Form.Label style={{color: 'white'}}>All:</Form.Label>
          <Form.Check type="checkbox" />
        </Form.Group>
      
        
        <Form.Group controlId="formPointTypeCheckboxes">
        <Form.Label style={{color: 'white'}}>Men:</Form.Label>
          <Form.Check type="checkbox" />
          <Form.Label style={{color: 'white'}}>Kote:</Form.Label>
          <Form.Check type="checkbox"  />
          <Form.Label style={{color: 'white'}}>Do:</Form.Label>
          <Form.Check type="checkbox" />
      
          <Form.Label style={{color: 'white'}}>Tsuki:</Form.Label>
          <Form.Check type="checkbox" />
          
      
          
        </Form.Group>
      
      
        <Form.Group controlId="formBasicCheckbox">
        <Form.Control as="select">
          <option key = 'blankChoice' hidden value> Select Group </option>
          <option>Men</option>
          <option>Ladies</option>
        
        </Form.Control>
        <br />
      
      
        <Form.Control as="select" >
        <option key = 'blankChoice' hidden value> Select Final Fraction </option>
          <option>Final Fraction</option>
        </Form.Control>
        <br />
        
        </Form.Group>
        <Button> Submit</Button>
      </Form>)
}
else if(props.type == "player"){
    return (<Form>
        <Form.Group controlId="formBasicEmail">
         
         <Form.Label style={{color: 'white'}}>Select Tournament:</Form.Label>
         <Form.Control type="tournament" placeholder="Enter requested players name" />
        
        </Form.Group>
        
        
        <Form.Group controlId="formBasicEmail">
         
         <Form.Label style={{color: 'white'}}>Select Opponent:</Form.Label>
         <Form.Control type="tournament" placeholder="Enter requested players name" />
        
        </Form.Group>
        
        <Form.Label style={{color: 'white'}}>Min Height:</Form.Label>
      
               <Slider />
          

           <Form.Label style={{color: 'white'}}>Max Height:</Form.Label>
       
               <Slider />
             
           
        <Form.Group controlId="formAllCheckbox">
        <Form.Label style={{color: 'white'}}>All:</Form.Label>
         <Form.Check type="checkbox" />
        </Form.Group>
        
        
        <Form.Group controlId="formPointTypeCheckboxes">
        <Form.Label style={{color: 'white'}}>Men:</Form.Label>
         <Form.Check type="checkbox" />
         <Form.Label style={{color: 'white'}}>Kote:</Form.Label>
         <Form.Check type="checkbox"  />
         <Form.Label style={{color: 'white'}}>Do:</Form.Label>
         <Form.Check type="checkbox" />
        
         <Form.Label style={{color: 'white'}}>Tsuki:</Form.Label>
         <Form.Check type="checkbox" />
         
        
         
        </Form.Group>
        
        
        <Button> Submit</Button>
        </Form>)
}
  
}
export default Filter