import React, {useState} from "react";
import {Col, Row, Card, Button, ButtonToolbar, ButtonGroup, Form} from 'react-bootstrap';
import RangeSlider from 'react-bootstrap-range-slider';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
function Slider(props){
    
    const [ value, setValue ] = useState(0); 
   
    return (
        <Form.Group as={Row}>
        <Col xs="9">
        <RangeSlider
        value={value}
        onChange={e => setValue(e.target.value)}
      />
        </Col>
        <Col xs="3">
          <Form.Control value={value}/>
        </Col>
      </Form.Group>
        
    )
}
export default Slider