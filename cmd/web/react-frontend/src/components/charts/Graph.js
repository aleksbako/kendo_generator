
import {Container, Col, Row, Card, Button, ButtonToolbar, ButtonGroup} from 'react-bootstrap';
import ChartSideBar from "./ChartSideBar";
import ChartBottom from "./ChartBottom";
import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip, Legend,ScatterChart, Scatter, BarChart, Bar, Cell, ResponsiveContainer, Pie, PieChart
} from 'recharts';

export default class Graph extends PureComponent {
    constructor(props){
      super(props);
      this.state = {data: props.data,extracted_data: props.extracted_data, type: "line", xAxis_keyType: "points", yAxis_keyType: "points", updated: false}

  }


 

renderPlots (type) {
    
  if(type == "scatter"){
  
  const lines = this.state.data.map((obj) => (
    <Scatter name={obj.countrydata[0].countries} data={obj.countrydata} fill="#8884d8" />
  ));
  return lines;
  
}
  else if (type == "lines"){
  
    const lines = this.state.data.map((obj, count) => (
    <Line
    key={count}
    name={obj.countrydata[0].countries}
    type="monotone"
    dataKey={"points"}
    stroke="#ff883c"
    strokeWidth={2}
    activeDot={{ r: 8 }}
  />
  
  ));
  return lines

  }
}


componentDidUpdate(){
    if(!this.state.updated){
    this.setState({
        data : this.props.data,
        extracted_data : this.props.extracted_data,
        updated: true
    })

}
}

  render() {
    

    var plot;
    
    
    if(this.state.type=="scatter"){
      
      plot = (<ScatterChart width={1050} height={350}
        margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey={this.state.xAxis_keyType} name="points" unit="cm" />
        <YAxis dataKey={this.state.yAxis_keyType} name="points" unit="cm" />
        {//<ZAxis dataKey="Do" range={[64, 144]} name="Do" unit="km" />
        }
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Legend />
        
        {this.renderPlots("scatter")}
    
       
      
        {//<Scatter name={this.state.data.country} data={this.state.data} fill="#8884d8" />
        }
        </ScatterChart>);

        
    }
    else if(this.state.type== "hist"){
      plot = (
      <BarChart
        width={1050}
        height={350}
        data={this.state.extracted_data}
        margin={{
          top: 20,
          right: 20,
          left: 10,
          bottom: 10,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="countries" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey={this.state.xAxis_keyType} fill="#8884d8" />
        {/*<BarChart><Bar dataKey="Kote" fill="#82ca9d" />
        <Bar dataKey="Do" fill="orange" />
      <Bar dataKey="Tsuki" fill="blue" />*/
      }
      </BarChart>
    )
    }

    else if(this.state.type == "pie"){
      plot = (<PieChart width={1050} height={350}>
        <Pie data={this.state.extracted_data} dataKey="points" cx="50%" cy="50%" outerRadius={60} fill="#8884d8" />
       { <Pie data={this.state.extracted_data} dataKey="points" cx="50%" cy="50%" innerRadius={70} outerRadius={90} fill="#82ca9d" label />
    }
      </PieChart>)
    }
    
    else{
      plot =   (<LineChart
      width={1050}
      height={350}
      data={this.state.extracted_data}
      margin={{
        top: 20, right: 10, left: 10, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="4 4" />
      <XAxis dataKey="countries" />
      <YAxis />
      <Tooltip />
      <Legend />
      {this.renderPlots("lines")}
      {/*<Line type="monotone" dataKey="points" stroke="red" activeDot={{ r: 8 }} />
      <Line type="monotone" dataKey="points" stroke="green"   activeDot={{ r: 8 }} />
      <Line type="monotone" dataKey="Do" stroke="blue" activeDot={{ r: 8 }}/>
    <Line type="monotone" dataKey="Tsuki" stroke="orange" activeDot={{ r: 8 }}/>*/}
    </LineChart>)
   
    }
    console.log(this.state.data)
    console.log(this.state.extracted_data)
    console.log("testing")
    return (
      <div>
        <Container fluid="md">
          <Row>
      <ChartSideBar type={this.props.categoryType} />
        
         
            
                <Col>
                <Row> <h1>{this.props.categoryType}</h1></Row>
                <Row>
                  <Col>
                
                <Card style={{ width: '12rem' }}>
  <Card.Body>
    <Card.Title>Total Number Of Men hits</Card.Title>
    <Card.Text>
      24
    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
<Col>
                <Card style={{ width: '12rem' }}>
  <Card.Body>
    <Card.Title>Total Number Of Kote hits</Card.Title>
    <Card.Text>
      22
    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
<Col>
                <Card style={{ width: '12rem' }}>
  <Card.Body>
    <Card.Title>Total Number Of Do hits</Card.Title>
    <Card.Text>
      10
    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
<Col>
                <Card style={{ width: '12rem' }}>
  <Card.Body>
    <Card.Title>Total Number Of Tsuki hits</Card.Title>
    <Card.Text>
      1
    </Card.Text>
  
  </Card.Body>
</Card>
</Col>
</Row>
<Row>
      
              {plot}
      </Row>
      
        </Col>
        
      
      </Row>
      <Row>
        <Col>

        
        <ButtonToolbar>
          <ButtonGroup>
        <Button onClick={() => this.setState({type: "scatter"})}  as="input" type="button" value="Scatter Plot"/>
        <Button onClick={() => this.setState({type: "line"})} as="input" type="button" value="Line Plot" />
        <Button onClick={() => this.setState({type: "hist"})} as="input" type="button" value="Histogram" />
        <Button onClick={() => this.setState({type: "pie"})} as="input" type="button" value="Pie Plot" />
        </ButtonGroup>
        </ButtonToolbar>
        </Col>

        
      </Row>
      <Row>
      
      <ChartBottom />
      </Row>
      </Container>
      
      </div>
    );
  }
}