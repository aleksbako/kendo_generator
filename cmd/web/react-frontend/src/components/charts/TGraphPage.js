
import React, { PureComponent } from 'react';
import Graph from './Graph';



 
export default class graphPage extends PureComponent {
  constructor(){
    super();
    this.state = {data: [],extracted_data: [], type: "line", xAxis_keyType: "points", yAxis_keyType: "points"}
}

componentWillMount(){
  
  var id = this.props.match.params.id
  console.log(id)
 
  let url = "http://localhost:8080/tChartInit/" + id;

  //Send the URL and fetch the data back from the server
  fetch(url)
              .then((data) => data.json())
              .then((text) => {
                console.log("request succeeded with JSON response", text);
                var focusedData = []
                text.forEach(obj =>
                  obj.countrydata.forEach(arrayElem =>
                    focusedData.push(arrayElem))
                    )
                this.setState({
                  data: text,
                  extracted_data: focusedData
                })
              })
              .catch(function (error) {
                console.log("request failed", error);
              });
              



}








  render() {
    return(<Graph data={this.state.data} extracted_data={this.state.extracted_data} categoryType="tournament"/>)
    
  }
}

