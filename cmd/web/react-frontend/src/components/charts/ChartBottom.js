import React, {Component} from "react";

import {Button, Navbar, Container, NavbarBrand, Row, Col, Form} from 'react-bootstrap';
var style = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "50px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%",
}
class ChartBottom extends Component{
    render() {
        return(
            <div >  
                <Navbar style={{style}} >
                    <Container>
                        <NavbarBrand>
                        <Form>
                        <Form.Label>README: Let the user pick how they wish to view the data that they have filtered out using the filter</Form.Label>
                            <Row>
                                <Col>




<Form.Group controlId="AxisChoice">
<Form.Label >Yaxis:</Form.Label>
  <Form.Control as="select">
  <option key = 'blankChoice' hidden value> Select Yaxis values </option>
    <option>Height</option>

  </Form.Control>
  <Form.Label >XAxis:</Form.Label>
  <Form.Control as="select">
  <option key = 'blankChoice' hidden value> Select Xaxis values </option>
    <option>Clubs</option>
  </Form.Control>
  </Form.Group>

                                
  <Form.Group controlId="formBasicEmail">
    
   
    <Form.Check  type="checkbox" label="Check me out" />
  </Form.Group>

  
  <Form.Group controlId="formBasicCheckbox">
  <Form.Control type="text" placeholder="Show amount of Players" readOnly />
  </Form.Group>


  </Col>

  </Row>
  
  
  
</Form>
<Button> Submit</Button>
                        </NavbarBrand>
                    </Container>
                </Navbar>
            </div>
        )
    }
}
export default ChartBottom;