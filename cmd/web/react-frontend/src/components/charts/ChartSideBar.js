import React, {useState} from "react";
import {Link } from "react-router-dom";
import {Button,Collapse} from 'react-bootstrap';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';

import Filter from "./Filter";

function ChartSideBar(props){
 
  const [open, setOpen] = useState(false);
  if(props.type == "tournament"){
return (
    <div>
      
         <nav class="d-none d-sm-block sidebar">
    <div className="sidebar-sticky">
      <ul className="nav flex-column">
        <h3 className=" d-flex justify-content-between align-items-center px-3 mt-4 mb-5 mx-3">Mock Ippon</h3>
        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-3">
          Categories:
        </h6>

        <li className="sidebar-item  pt-2 pb-2">
        <Link to="/data"><a className="dropdown-item active" id="players" href="#">Player</a> </Link>
        </li>
        <li className="sidebar-item pt-2 pb-2">
        <Link to="/data/tournaments"> <a className="dropdown-item" id="tournament">Tournament</a> </Link>
        </li>
        <li className="sidebar-item pt-2 pb-2">
          <a className="dropdown-item" id="match" href="#">Match
          </a>
        </li>
  
         <Button
        onClick={() => setOpen(!open)}
        aria-controls="example-collapse-text"
        aria-expanded={open}
      >
        Filter
      </Button>
        <Collapse in={open}>
        <Filter type="tournament"/>
</Collapse>
      </ul>


    </div>
  </nav>
  <div>
    <i class="fas fa-user"></i>
    </div>
    </div>
);
  }
  else if(props.type == "player"){
return (<div>
      
      <nav class="d-none d-sm-block sidebar">
 <div className="sidebar-sticky">
   <ul className="nav flex-column">
     <h3 className=" d-flex justify-content-between align-items-center px-3 mt-4 mb-5 mx-3">Mock Ippon</h3>
     <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-3">
       Categories:
     </h6>

     <li className="sidebar-item  pt-2 pb-2">
     <Link to="/data"><a className="dropdown-item active" id="players" href="#">Player</a> </Link>
     </li>
     <li className="sidebar-item pt-2 pb-2">
     <Link to="/data/tournaments"> <a className="dropdown-item" id="tournament">Tournament</a> </Link>
     </li>
     <li className="sidebar-item pt-2 pb-2">
       <a className="dropdown-item" id="match" href="#">Match
       </a>
     </li>

      <Button
     onClick={() => setOpen(!open)}
     aria-controls="example-collapse-text"
     aria-expanded={open}
   >
     Filter
   </Button>
     <Collapse in={open}>
     <Filter type="player"/>
</Collapse>
   </ul>


 </div>
</nav>
<div>
 <i class="fas fa-user"></i>
 </div>
 </div>
);
  }
}
export default ChartSideBar;