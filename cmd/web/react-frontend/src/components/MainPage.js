import React, {Component} from "react";
import {Link } from "react-router-dom";
import {Container, Col, Row} from 'react-bootstrap';
import NavigationBar from "./NavigationBar"
import { render } from "react-dom";
import MainCards from "./MainCards";
function MainPage() {
    const styles = {
     
        backgroundImage: "url(/images/front.png)"
    }
     return (
         <div>
        <NavigationBar />
    <div className=" jumbotron jumbotron-fluid jumbotron-image" style={styles} >
        <Container fluid="md">
        <span className="dot"></span>
        <h1 className="display-3 pt-3">Mock Ippon</h1>
        <Col>
        <p class="lead" id="subtitle">Follow your performance, and improve your imperfections.</p>
        </Col>

</Container>
</div>
<div>
        <Container fluid="md">
    <div className="mx-5">
      <div className="row">
        <div className="col-sm-12">
          <h3>Lorem ipsum dolor</h3>
        </div>
  </div>
  </div>
  <MainCards 
  title="Tournament Generator"
   description="Need to generate a competition really quickly? Use our generator
   feature to quickly
   and
   easily generate tournament layout"
   urlLink="/generate"
   buttonText="Generate Tournament"
   />

  <MainCards 
  title="Competition Analysis" 
  description="Wish to analyse your own performance and see how much you have improved
  since your
  last
  competition. Check out our competition analysis feature!"
  urlLink="/data"
  buttonText="Go and Analyse"
  />

</Container>
</div>
          </div>
     );
     
 }

 export default MainPage;