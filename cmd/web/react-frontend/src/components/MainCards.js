import React from "react";
import {Container, Col, Row} from 'react-bootstrap';

function MainCards(props){

    return (
        <div className="mb-5 pb-5">
        <Row >
          <Col >
            <h4 className="pt-5 pb-4">{props.title}</h4>
            <p className="pb-5" id="description">{props.description}</p>
            <a href={props.urlLink} className="btn btn-custom">{props.buttonText}</a>
          </Col>
          <Col>
            <img></img>
          </Col>
        </Row>
        </div>
    )
}

export default MainCards