import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, useParams } from "react-router-dom";
import './index.css';

import reportWebVitals from './reportWebVitals';

import TsummaryPage from "./components/SummaryPages/TsummaryPage";
import MainPage from "./components/MainPage";
import DataPlayer from "./components/data/DataPlayer";
import DataTournament from './components/data/DataTournament';
import TGraphPage from "./components/charts/TGraphPage";
import PGraphPage from "./components/charts/PGraphPage";
import DataMain from "./components/data/DataMain"
import PsummaryPage from "./components/SummaryPages/PsummaryPage"


const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={MainPage} />
      <Route exact path="/data" component={DataMain} />
      <Route exact path="/data/players" component={DataPlayer} />
      <Route exact path="/data/tournaments" exact component={DataTournament} />
      <Route exact path="/data/tournaments/specific/:id" component={TsummaryPage} />
      <Route exact path="/data/players/specific/:id" component={PsummaryPage} />
      <Route exact path="/data/players/specific/graph/:id" component={PGraphPage} />
      <Route exact path="/data/tournaments/specific/graph/:id" component={TGraphPage} />
    </Switch>
  </BrowserRouter>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
