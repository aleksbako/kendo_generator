package main

// here we start the project
import (
	"net/http"

	"github.com/labstack/echo/v4"
	echoMw "github.com/labstack/echo/v4/middleware"
	routes "gitlab.com/aleksbako/kendo_generator/cmd/Routes"
)

func main() {
	e := echo.New()
	e.Use(echoMw.Logger())
	e.Use(echoMw.Gzip())
	e.Use(echoMw.StaticWithConfig(echoMw.StaticConfig{
		Root:   "web/react-frontend/build/", // This is the path to your SPA build folder, the folder that is created from running "npm build"
		Index:  "index.html",                // This is the default html page for your SPA
		Browse: false,
		HTML5:  true,
	}))
	fs := http.FileServer(http.Dir("web/react-frontend/build/"))
	e.GET("/*", echo.WrapHandler(http.StripPrefix("/", fs)))
	e.GET("/data", echo.WrapHandler(http.StripPrefix("/data", fs)))
	e.GET("/data/players", echo.WrapHandler(http.StripPrefix("/data/players", fs)))
	e.GET("/data/players/specific/:id", echo.WrapHandler(http.StripPrefix("/data/players/specific/:id", fs)))
	e.GET("/data/tournaments", echo.WrapHandler(http.StripPrefix("/data/tournaments", fs)))
	e.GET("/data/tournaments/specific/:id", echo.WrapHandler(http.StripPrefix("/data/tournaments/specific/:id", fs)))
	e.GET("/data", echo.WrapHandler(http.StripPrefix("/data", fs)))

	e.GET("/getallplayers", routes.RequestAllPlayers)
	e.GET("/getalltournaments", routes.RequestAllTournaments)
	e.GET("/getplayer/:id", routes.RequestSpecificPlayer)
	e.GET("/gettournament/:id", routes.RequestSpecificTournament)
	e.GET("/tChartInit/:id", routes.TournamenCharttInit)
	e.Logger.Fatal(e.Start(":8080"))
	/*



		//redirect /bootstrap-4.3.1-dist/css/ request to find the required files in the /web/bootstrap-4.3.1-dist/css folder
		router.PathPrefix("/bootstrap-4.3.1-dist/css/").Handler(http.StripPrefix("/bootstrap-4.3.1-dist/css/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/css/"))))
		router.PathPrefix("/data/bootstrap-4.3.1-dist/css/").Handler(http.StripPrefix("/data/bootstrap-4.3.1-dist/css/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/css/"))))
		router.PathPrefix("/data/tournament/bootstrap-4.3.1-dist/css/").Handler(http.StripPrefix("/data/tournament/bootstrap-4.3.1-dist/css/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/css/"))))
		router.PathPrefix("/getplayer/bootstrap-4.3.1-dist/css/").Handler(http.StripPrefix("/getplayer/bootstrap-4.3.1-dist/css/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/css/"))))
		//redirect /bootstrap-4.3.1-dist/js/ request to find the required files in the /web/bootstrap-4.3.1-dist/js folder
		router.PathPrefix("/bootstrap-4.3.1-dist/js/").Handler(http.StripPrefix("/bootstrap-4.3.1-dist/js/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/js/"))))
		router.PathPrefix("/data/bootstrap-4.3.1-dist/js/").Handler(http.StripPrefix("/data/bootstrap-4.3.1-dist/js/", http.FileServer(http.Dir("web//bootstrap-4.3.1-dist/js/"))))

		router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("web/images"))))
		//redirect /style/ request to find the required files in the /web/style folder
		router.PathPrefix("/style/").Handler(http.StripPrefix("/style/", http.FileServer(http.Dir("web/style"))))
		api.PathPrefix("/style/").Handler(http.StripPrefix("/style/", http.FileServer(http.Dir("web/react-frontend/src/components/style"))))
		//redirect /script/ request to find the required files in the /web/script folder /style/datagen.css
		router.PathPrefix("/script/").Handler(http.StripPrefix("/script/", http.FileServer(http.Dir("web/script"))))

	*/
}
