# Kendo Match Generator / Analysis platform.

## Introduction:

The project is based on a simple idea of organizing matches such that people in competitions compete against other people from people form other countries and if that is not possible then they are put against people from other clubs.

Once the competition has been finish the data from all of the matches is gathered and then put into the database such that it could be used for analysis purposes later on.

## Things to keep in mind:

### Frontend:

- Easy to navigate.
- Display all the data that the user requests or inputs.
- Do not make the user wait unnecessarily long for data

### Server:

- Check if the data provided by the user is  not malicious.
- Filter all data provided by the user.
- Format the data requested by the user from the database.
- Test everything to decrease amount of bugs that may occur

### Database:

- All the tables be organized properly with correct Primary and foreign keys.
- Setup trigger to make the processing time on the server much shorter.
- Build such that it may be easier to expand



## More detailed description of each part:

### Frontend:

#### Main page:

- The user has to be able to navigate the page with little to no trouble
- Provides a short description of all the things that user might find on our page
- UI that is familiar, but also pleasing to the user's eyes

#### Generator page:

- Simple but appealing UI.
- Display all the players added in a neat way such that they can be removed if necessary.
- The generate button loads in a competition setup which is fetched in accordance to the servers decision.

#### Analysis page:

- List of things that can be requested from the database indirectly.
- Have a option where the user can input the things they require as well, i.e. having just the players age and the types of points they got during the competition.
- Add graphs and other types of visual forms of analyzing data.

### Backend:

#### Server:

- Is running a Go web-server
- Has requests that communicate between both the database and the fronted.
- All data is sanitized and formated to the appropriate format 
- Has cookies such that multiple user can access the website at any given time.
- Handles all data and secures things which should not be accessed by users who do not have the authority to do so.
- May even has data if it is required.

#### Database:

- Data is easily accessibly by the server
- Have triggers set up to keep data going in and out fast and correct.
- Decrease amount of problems by making the database scalable.