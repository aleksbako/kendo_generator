## UserStories

#### Main Page:
- As a user, I would like have a top bar, so that the navigation between pages would easier.
- As a user, I would like that it would be possible to hide the top bar to be hidden away, so that it would not get in the way when not used.
- As a user, I would like to have a short but detailed description of each page, so that i would know what each page is meant for without being bored too quickly.
- As a user, I would like to have images of the sport, so that it would make me feel like this is a website meant specifically for this.
- As a user, I would like to have the ability to change my page into dark mode, so that it would not feel as exhausting for my eyes.

#### Generator Page:

- As a user, I would like to be able to add all the users in the generator.
- As a user, I would like to auto-complete things as i fill them out if they had been already typed, so that my experience would be much more enjoyable.
- As a user, I would like to have a counter on the side that shows how many players have i added so far, so that it would be easier for me to keep track of how many people i have added.
- as a user, I would like to be able to import an Excel file, such that if i have a file ready i can just simply import it without having to retype all of the data.
- as a user, I would like to see the possible layouts for the matches before i generate it based on the amount of players currently provided, so that i feel like i have more control over it and it it is more interesting than just seeing fill-out text bars.

#### Data Analysis Page:

- As a user, I would like to be able to search for my data with as little typing as possible and by selecting the things i would like to see with a click of a button.
- As a user, I would like to be able to import my Excel file, so that it would reduce the amount of time i have to spend writing down all the information into the browser.
- As a user, I would like to have the ability to choose between text and graph options as to how the data should be displayed, so that i can see the data the way i choose to.
- As a user, I would like to see an overview of all the data currently in the database for certain cases when entering the page, so that i could find some data without requiring to doing an actual search.
- As a user i would like to login and see my own match results shown if it the data exists, so that i would be able to evaluate my own performance.
- As a user i would like to be able to reconstruct a whole competition in the same format it was generated, but with all of the data of what had occured and all the results during the matches, because this would give me a more detailed and visual description if i was curious.

