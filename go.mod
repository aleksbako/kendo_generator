module gitlab.com/aleksbako/kendo_generator

go 1.12

require (
	github.com/bearbin/go-age v0.0.0-20140407072555-316d0c1e7cd1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.3.0
	github.com/lib/pq v1.7.0
	github.com/rakyll/statik v0.1.7 // indirect
	github.com/yuin/goldmark v1.1.31 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/tools v0.0.0-20200530233709-52effbd89c51 // indirect
)
