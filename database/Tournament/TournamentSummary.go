package databT

import (
	"fmt"

	datab "gitlab.com/aleksbako/kendo_generator/database"
	databM "gitlab.com/aleksbako/kendo_generator/database/Match"
	databP "gitlab.com/aleksbako/kendo_generator/database/Player"
)

//GetTournament - fetches the Tournament data from the database and returns a Tournament object.
func GetTournament(id int) TournamentJSONWithAllData {
	//Open connection with database.
	db := datab.OpenDbConnection()

	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `SELECT	* FROM "Tournament" WHERE "T_Id" = $1;`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}

	//Close after function is finished.
	defer db.Close()
	var tournamentobj TournamentJSONWithAllData

	//Go through the rows and print out the values. Return them in an object in the future.
	for rows.Next() {
		var tID int
		var name string
		var winnerID int
		var year int
		matchlist := databM.GetTIDMatches(id)
		playeramount := countTplayers(id)
		countryamount := countTcountries(id)
		er = rows.Scan(&tID, &name, &winnerID, &year)
		winner := databP.GetPlayer(winnerID)
		piechartData := pointTypeSummary(tID)
		fmt.Printf("%3v | %8v | %3v | %3v\n", tID, name, winnerID, year)
		tournamentobj = TournamentJSONWithAllData{tID, name, winnerID, winner.Name, year, matchlist, playeramount, countryamount, piechartData}

	}
	fmt.Println(tournamentobj)
	return tournamentobj

}

func countTplayers(id int) int {

	db := datab.OpenDbConnection()

	sqlStatement := `SELECT count(*) FROM (Select distinct(point."Player_id") from "Match" matches join "Points" point on matches."Match_Id" = point."Match_Id" where matches."T_Id" = $1 group by point."Player_id") as amount;`

	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()
	var amount int
	for rows.Next() {

		err := rows.Scan(&amount)
		if err != nil {
			panic(err)
		}

	}

	return amount

}

func countTcountries(id int) int {

	db := datab.OpenDbConnection()

	sqlStatement := `select count(*) from (Select distinct(players.country) from "Match" join (Select "player_id", country from "Player") players on "Match"."W_Player_Id" = players."player_id" or "Match"."R_Player_Id" = players."player_id" where "T_Id"=$1) as Country;`

	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()
	var amount int
	for rows.Next() {

		err := rows.Scan(&amount)
		if err != nil {
			panic(err)
		}

	}

	return amount

}

//pointTypeSummary - Get all type of point sums from a given tournament. Return PieChartPointData.
func pointTypeSummary(tid int) PieChartPointData {

	db := datab.OpenDbConnection()

	sqlStatement := `SELECT SUM("Men_Points") AS sum_men, SUM("Kote_Points")AS sum_kote, SUM("Do_Points") AS sum_do, SUM("Tsuki_Points") AS sum_tsuki
	FROM "Player" player
	JOIN "Match" matches on (matches."W_Player_Id" = player.player_id ) or (matches."R_Player_Id" = player.player_id)
	JOIN "Tournament" tournament on (matches."T_Id" = tournament."T_Id")
	JOIN "Points" points on player.player_id = points."Player_id" and matches."Match_Id" = points."Match_Id"
	where tournament."T_Id" = $1 group by tournament."T_Id";`

	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, tid)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()
	var piechartData PieChartPointData
	for rows.Next() {
		var Men int
		var Kote int
		var Do int
		var Tsuki int
		err := rows.Scan(&Men, &Kote, &Do, &Tsuki)
		if err != nil {
			panic(err)
		}
		piechartData = PieChartPointData{Men, Kote, Do, Tsuki}

	}

	return piechartData
}
