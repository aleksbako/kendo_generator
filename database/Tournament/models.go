package databT

import (
	databM "gitlab.com/aleksbako/kendo_generator/database/Match"
)

type TournamentInit struct {
	Name        string          `json:"t_name,string"`
	CountryData []CountryPoints `json:"countrydata,string"`
}
type CountryPoints struct {
	Country string `json:"countries,string"`
	Count   int    `json:"points,string"`
}

//TournamentJSON format to export to databse
type TournamentJSON struct {
	TID      int    `json:"t_id,string"`
	Name     string `json:"name"`
	WinnerID int    `json:"winner_id,string"`
	Year     int    `json:"year,string"`
}
type TournamentJSONWithAllData struct {
	TID           int                   `json:"t_id,string"`
	Name          string                `json:"name"`
	WinnerID      int                   `json:"winner_id,string"`
	WinnerName    string                `json:"winner_name,string"`
	Year          int                   `json:"year,string"`
	Matches       []databM.MatchPlayers `json:"matches,string"`
	PlayerAmount  int                   `json:"pAmount,string"`
	CountryAmount int                   `json:"cAmount,string"`
	PiechartData  PieChartPointData     `json:"pieData,string"`
}

//PieChartPointData - struct use to transport data for tournament summary piechart.
type PieChartPointData struct {
	Men   int `json:"men,string"`
	Kote  int `json:"kote,string"`
	Do    int `json:"do,string"`
	Tsuki int `json:"tsuki,string"`
}
