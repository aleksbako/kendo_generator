package databT

import (
	"fmt"

	datab "gitlab.com/aleksbako/kendo_generator/database"
)

func GetChartInitialTournament(id int) []TournamentInit {

	db := datab.OpenDbConnection()

	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `SELECT	tournament.name ,player.country ,count("Total_Points")  from 
	((Select *  from "Tournament" join "Match" on "Match"."T_Id" = "Tournament"."T_Id" WHERE "Tournament"."T_Id" = $1  ) as tournament
	join (Select player_id ,country, name from "Player" ) as player on tournament."W_Player_Id" = player.player_id )
	join (Select "Match_Id", "Total_Points", "Player_id" from "Points") as points on player.player_id = points."Player_id" group by country, tournament.name;`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}

	//Close after function is finished.
	defer db.Close()
	var tournamentInitList []TournamentInit
	var countryInitList []CountryPoints

	//Go through the rows and print out the values. Return them in an object in the future.
	for rows.Next() {

		var name string
		var country string
		var amount int

		er = rows.Scan(&name, &country, &amount)

		fmt.Printf("%3v | %8v | %3v \n", name, country, amount)
		countryData := CountryPoints{country, amount}
		countryInitList := append(countryInitList, countryData)
		tournamentInitObj := TournamentInit{name, countryInitList}
		tournamentInitList = append(tournamentInitList, tournamentInitObj)
	}
	return tournamentInitList
}
