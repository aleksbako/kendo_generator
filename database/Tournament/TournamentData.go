package databT

import (
	"fmt"

	_ "github.com/lib/pq"
	datab "gitlab.com/aleksbako/kendo_generator/database"
)

func GetAllTournaments() []TournamentJSON {
	//Open connection with database.

	db := datab.OpenDbConnection()

	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `SELECT * FROM "Tournament"`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()

	var tournamentlist []TournamentJSON
	for rows.Next() {
		var tID int
		var name string
		var winnerID int
		var year int
		er = rows.Scan(&tID, &name, &winnerID, &year)

		tournamentobj := TournamentJSON{tID, name, winnerID, year}
		tournamentlist = append(tournamentlist, tournamentobj)
	}
	fmt.Println(tournamentlist)
	return tournamentlist
}
