package databP

type PlayerJSON struct {
	PlayerID  int    `json:"player_id,string"`
	Name      string `json:"name"`
	Country   string `json:"country"`
	Club      string `json:"club"`
	Grade     string `json:"grade"`
	Height    int    `json:"height,string"`
	BirthDate string `json:"birth_date"`
}

// TournamentMatchSpecificPlayerJSON object
type TournamentMatchSpecificPlayerJSON struct {
	TournamentName string `json:"tournament_name"`
	Year           int    `json:"year,string"`
	TypeMatch      string `json:"type_match"`
	CompTypeMatch  string `json:"comp_type_match"`
	OpponentName   string `json:"opponent_name"`
}
