package databP

import (
	"strconv"
	"time"

	"github.com/bearbin/go-age"
	datab "gitlab.com/aleksbako/kendo_generator/database"
)

//GetPlayer - fetches the player data from the database and returns a player object.
func GetPlayer(id int) PlayerJSON {
	//Open connection with database.
	db := datab.OpenDbConnection()

	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `SELECT * FROM "Player" WHERE player_id = $1;`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()

	var playerobj PlayerJSON
	//Go through the rows and print out the values. Return them in an object in the future.
	for rows.Next() {
		var playerID int
		var name string
		var date time.Time
		var country string
		var club string
		var grade string
		var height int

		er = rows.Scan(&playerID, &name, &country, &club, &grade, &height, &date)

		// Calculate and output age
		calcAge := age.Age(date)
		age := strconv.Itoa(calcAge)
		//Output player object
		playerobj = PlayerJSON{playerID, name, country, club, grade, height, age}
	}
	return playerobj
}

//GetSpecificPlayerTournamentsMatches return match an tournament information given a player id
func GetSpecificPlayerTournamentsMatches(id int) []TournamentMatchSpecificPlayerJSON {

	db := datab.OpenDbConnection()

	sqlStatement := `SELECT PlayerTournaments.name AS tournament_name, year, "Type", "CompType", OpponentName
	FROM(
	(SELECT "Match"."T_Id", "Tournament"."T_Id", "Tournament".name, winner_id, year, "Type", "CompType", "Player".name AS OpponentName,
	 CASE
		 WHEN "W_Player_Id" = $1 THEN "R_Player_Id"
		 WHEN "R_Player_Id" = $1 THEN "W_Player_Id"
	 END 
	 FROM "Match" 
	 JOIN "Tournament"  ON "Tournament"."T_Id" = "Match"."T_Id"
	 JOIN "Player" ON "Match"."W_Player_Id" = "Player".player_id or "Match"."R_Player_Id" = "Player".player_id
	 WHERE "W_Player_Id" = $1 OR "R_Player_Id" = $1)) AS PlayerTournaments;`

	rows, er := db.Query(sqlStatement, id)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()

	var playerDataList []TournamentMatchSpecificPlayerJSON
	//Go through the rows and print out the values. Return them in an object in the future.
	for rows.Next() {
		var tournamentName string
		var year int
		var typeMatch string
		var compTypeMatch string
		var opponentName string

		er = rows.Scan(&tournamentName, &year, &typeMatch, &compTypeMatch, &opponentName)
		//Calculate age
		var playerDataObj TournamentMatchSpecificPlayerJSON
		playerDataObj = TournamentMatchSpecificPlayerJSON{tournamentName, year, typeMatch, compTypeMatch, opponentName}
		playerDataList = append(playerDataList, playerDataObj)
	}

	return playerDataList
}
