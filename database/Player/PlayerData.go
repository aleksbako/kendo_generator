package databP

import (
	"strconv"
	"time"

	"github.com/bearbin/go-age"
	datab "gitlab.com/aleksbako/kendo_generator/database"
)

//GetAllPlayers - fetches all players in the DB list
func GetAllPlayers() []PlayerJSON {
	//Open connection with database.
	db := datab.OpenDbConnection()

	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `SELECT * FROM "Player"`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()

	var playerlist []PlayerJSON
	//Go through the rows and print out the values. Return them in an object in the future.
	for rows.Next() {
		var playerID int
		var name string
		var date time.Time
		var country string
		var club string
		var grade string
		var height int

		er = rows.Scan(&playerID, &name, &country, &club, &grade, &height, &date)

		//Calculate age
		calcAge := age.Age(date)
		age := strconv.Itoa(calcAge)
		//Output each object into the playerlist array(with age isntead of birth_date)
		var playerobj PlayerJSON
		playerobj = PlayerJSON{playerID, name, country, club, grade, height, age}
		playerlist = append(playerlist, playerobj)
	}

	return playerlist
}
