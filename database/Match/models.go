package databM

type MatchPlayers struct {
	MatchID      int    `json:"match_id,string"`
	Wplayername  string `json:"white_player"`
	Rplayername  string `json:"red_player"`
	Typename     string `json:"typename"`
	CompTypename string `json:"comptypename"`
}
