package databM

import (
	datab "gitlab.com/aleksbako/kendo_generator/database"
	databP "gitlab.com/aleksbako/kendo_generator/database/Player"
)

func GetTIDMatches(tID int) []MatchPlayers {
	//Open connection with database.

	db := datab.OpenDbConnection()
	//The query statement which has room to be filled out with provided paramenters.
	sqlStatement := `Select * from "Match" where "T_Id" = $1`
	//Executes the Select query in postgresql database.
	rows, er := db.Query(sqlStatement, tID)
	if er != nil {
		panic(er)
	}
	//Close after function is finished.
	defer db.Close()

	var matcheslist []MatchPlayers
	for rows.Next() {
		var mid int
		var tid int
		var wplayerid int
		var rplayerid int
		var wpointid int
		var rpointid int
		var typename string
		var ctname string

		er = rows.Scan(&mid, &tid, &wplayerid, &rplayerid, &wpointid, &rpointid, &typename, &ctname)
		wplayer := databP.GetPlayer(wplayerid)
		rplayer := databP.GetPlayer(rplayerid)

		matchobj := MatchPlayers{mid, wplayer.Name, rplayer.Name, typename, ctname}
		matcheslist = append(matcheslist, matchobj)
	}

	return matcheslist
}
